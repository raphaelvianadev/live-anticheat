package br.com.raphaelfury.anticheat.cheater;

import java.util.ArrayList;
import java.util.UUID;

import br.com.raphaelfury.anticheat.alert.Alert;

public class Cheater {
	
	private ArrayList<Alert> alerts;
	private UUID uuid;
	private Long delay;
	
	public Cheater(UUID uuid) {
		this.alerts = new ArrayList<>();
		this.uuid = uuid;
		this.delay = 0L;
	}
	
	public ArrayList<Alert> getAlerts() {
		return alerts;
	}
	
	public void setAlerts(ArrayList<Alert> alerts) {
		this.alerts = alerts;
	}
	
	public UUID getUUID() {
		return uuid;
	}
	
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	public Long getDelay() {
		return delay;
	}
	
	public void addDelay() {
		delay = System.currentTimeMillis() + 1500;
	}
	
	public boolean hasDelay() {
		return (System.currentTimeMillis() - delay) < 0;
	}
	
	public void setDelay(Long delay) {
		this.delay = delay;
	}
	
	public void addAlert(Alert alert) {
		alerts.add(alert);
	}
	
	public void removeAlert(Alert alert) {
		alerts.remove(alert);
	}
	
}
