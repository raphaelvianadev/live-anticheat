package br.com.raphaelfury.anticheat.load;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import br.com.raphaelfury.anticheat.manager.Manager;
import br.com.raphaelfury.anticheat.util.ClassGetter;


public class LoadManager {
	
	private Manager manager;

	public LoadManager(Manager manager) {
		this.manager = manager;
	}
	
	public void load() {

		/* Registering the listener classes */

		for (Class<?> classes : ClassGetter.getClassesForPackage(manager.getPlugin(), "br.com.raphaelfury.anticheat")) {
			try {
				Listener listener = null;
				if (Listener.class.isAssignableFrom(classes)) {
					listener = (Listener) classes.getConstructor(Manager.class).newInstance(manager);
				}
				if (listener == null)
					continue;
				Bukkit.getPluginManager().registerEvents(listener, manager.getPlugin());
				System.out.println("Listener: " + classes.getSimpleName() + " loaded!");
			} catch (Exception exception) {
				manager.getPlugin().getLogger().info("[" + manager.getPlugin().getName() + "] Error loading Listener: " + classes.getSimpleName() + "!\n" +  exception);
			}
			
		}
		
	}

}
