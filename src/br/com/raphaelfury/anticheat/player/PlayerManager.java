package br.com.raphaelfury.anticheat.player;

import java.util.HashMap;
import java.util.UUID;

public class PlayerManager {
	
	private HashMap<UUID, PlayerData> players = new HashMap<>();

	public HashMap<UUID, PlayerData> getPlayers() {
		return players;
	}

	public PlayerData getPlayer(UUID uuid) {
		return players.get(uuid);
	}
	
	public PlayerData getPlayer(PlayerData player) {
		return players.get(player.getUUID());
	}
	
	public void addPlayer(UUID uuid) {
		players.put(uuid, new PlayerData(uuid));
	}
	
	public void removePlayer(UUID uuid) {
		players.remove(uuid);
	}

}
