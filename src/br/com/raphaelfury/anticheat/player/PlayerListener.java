package br.com.raphaelfury.anticheat.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import br.com.raphaelfury.anticheat.manager.Manager;

public class PlayerListener implements Listener {
	
	private Manager manager;
	private PlayerManager playerManager;
	
	public PlayerListener(Manager manager) {
		this.manager = manager;
		playerManager = this.manager.getPlayerManager();
	}
	
	@EventHandler
	private void onPlayerJoin(PlayerJoinEvent event) {
		playerManager.addPlayer(event.getPlayer().getUniqueId());
	}
	
	@EventHandler
	private void onPlayerQuit(PlayerQuitEvent event) {
		playerManager.removePlayer(event.getPlayer().getUniqueId());
	}
}
