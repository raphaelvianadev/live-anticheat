package br.com.raphaelfury.anticheat.player;

import java.util.UUID;

public class PlayerData {
	
	private UUID uuid;
	private Boolean alerts;
	
	public PlayerData(UUID uuid) {
		this.uuid = uuid;
		this.alerts = true;
	}
	
	public void setAlerts(Boolean alerts) {
		this.alerts = alerts;
	}
	
	public Boolean getAlerts() {
		return alerts;
	}
	
	public UUID getUUID() {
		return uuid;
	}

}
