package br.com.raphaelfury.anticheat.manager;

import org.bukkit.plugin.java.JavaPlugin;

import br.com.raphaelfury.anticheat.LiveAntiCheat;
import br.com.raphaelfury.anticheat.alert.AlertManager;
import br.com.raphaelfury.anticheat.load.LoadManager;
import br.com.raphaelfury.anticheat.player.PlayerManager;

public class Manager {
	
	private LiveAntiCheat plugin;
	private PlayerManager playerManager;
	private AlertManager alertManager;
	
	public Manager() {
		this.plugin = JavaPlugin.getPlugin(LiveAntiCheat.class);
		this.playerManager = new PlayerManager();
		this.alertManager = new AlertManager();
		
		new LoadManager(this).load();
	}
	
	public LiveAntiCheat getPlugin() {
		return plugin;
	}

	public PlayerManager getPlayerManager() {
		return playerManager;
	}
	
	public AlertManager getAlertManager() {
		return alertManager;
	}

}
