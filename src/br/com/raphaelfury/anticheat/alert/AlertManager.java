package br.com.raphaelfury.anticheat.alert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import br.com.raphaelfury.anticheat.cheater.Cheater;
import br.com.raphaelfury.anticheat.hack.Hack;
import br.com.raphaelfury.anticheat.hack.Probability;

public class AlertManager {

	private HashMap<UUID, ArrayList<Alert>> alerts = new HashMap<>();
	private HashMap<UUID, Cheater> cheaters = new HashMap<>();

	public void addAlert(UUID uuid, Hack hackType, Probability probability) {
		Integer warnings = 1;

		if (cheaters.containsKey(uuid) && cheaters.get(uuid).hasDelay())
			return;

		if (!containsAlert(uuid, hackType)) {
			getCheater(uuid).getAlerts().add(new Alert(1, uuid, hackType));
			cheaters.get(uuid).addDelay();
		} else {
			ArrayList<Alert> alertsCheater = getCheater(uuid).getAlerts();
			
			for (int i = 0; i < alertsCheater.size(); i++) {
				Alert alert = alertsCheater.get(i);

				if (alert.getHackType().equals(hackType)) {
					alert.addAlert();
					warnings = alert.getAlerts();
					cheaters.get(uuid).addDelay();
				}
			}
		}
	}

	public Alert getAlertByHack(UUID uuid, Hack hack) {
		for (int i = 0; i < alerts.get(uuid).size(); i++) {
			Alert alert = alerts.get(uuid).get(i);

			if (alert.getHackType().equals(hack))
				return alert;
		}

		return null;

	}

	public void clearAlerts(UUID uuid) {
		alerts.remove(uuid);
	}

	public boolean containsAlert(UUID uuid, Hack hack) {
		boolean contains = false;

		if (!cheaters.containsKey(uuid))
			return false;

		for (Alert alerts : cheaters.get(uuid).getAlerts()) {
			if (alerts.getHackType().equals(hack))
				contains = true;
		}

		return contains;
	}

	public void sendAlert(String message) {
		for (Player players : Bukkit.getOnlinePlayers()) {
			if (players.hasPermission("live.message"))
				players.sendMessage("§eLiveAntiCheat§7: " + message);
		}
	}

	public HashMap<UUID, Cheater> getAllCheaters() {
		return cheaters;
	}

	public Cheater getCheater(UUID uuid) {
		if (!cheaters.containsKey(uuid))
			cheaters.put(uuid, new Cheater(uuid));

		return cheaters.get(uuid);
	}

	public void setCheaters(HashMap<UUID, Cheater> cheaters) {
		this.cheaters = cheaters;
	}

}
