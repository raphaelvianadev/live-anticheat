package br.com.raphaelfury.anticheat.alert;

import java.util.UUID;

import br.com.raphaelfury.anticheat.hack.Hack;

public class Alert {
	
	private Integer alerts;
	private UUID uuid;
	private Hack hackType;
	
	public Alert(Integer alerts, UUID uuid, Hack hackType) {
		this.setAlerts(alerts);
		this.uuid = uuid;
		this.hackType = hackType;
	}
	
	public Integer getAlerts() {
		return alerts;
	}
	
	public UUID getUUID() { 
		return uuid;
	}
	
	public Hack getHackType() {
		return hackType;
	}

	public void setAlerts(Integer alerts) {
		this.alerts = alerts;
	}
	
	public void addAlert() {
		alerts++;
	}
	
	public void clearAlerts() {
		alerts = 0;
	}

}
