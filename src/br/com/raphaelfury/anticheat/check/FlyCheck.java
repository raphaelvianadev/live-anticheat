package br.com.raphaelfury.anticheat.check;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import br.com.raphaelfury.anticheat.LiveAntiCheat;
import br.com.raphaelfury.anticheat.alert.AlertManager;
import br.com.raphaelfury.anticheat.hack.Hack;
import br.com.raphaelfury.anticheat.hack.Probability;
import br.com.raphaelfury.anticheat.movement.MoveTypes;

public class FlyCheck implements Listener {
	private HashMap<UUID, Integer> ticks = new HashMap<UUID, Integer>();

	@EventHandler
	private void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		AlertManager alert = LiveAntiCheat.getManager().getAlertManager();

		if (player.isInsideVehicle() || player.getGameMode().equals(GameMode.CREATIVE)
				|| MoveTypes.isClimbing(player.getLocation()) || player.getAllowFlight())
			return;

		if (!ticks.containsKey(player.getUniqueId()))
			ticks.put(player.getUniqueId(), 0);

		Integer tick = ticks.get(player.getUniqueId());

		if (!MoveTypes.hasSurrondingBlocks(player.getLocation().getBlock())
				&& !MoveTypes.isSwimming(player.getLocation())) {
			if (!MoveTypes.isFalling(event)) {
				tick++;
				ticks.put(player.getUniqueId(), tick);
			} else {
				ticks.put(player.getUniqueId(), 0);
			}
		} else {
			ticks.put(player.getUniqueId(), 0);
		}

		if (tick >= 6 && tick <= 12) {
			alert.addAlert(player.getUniqueId(), Hack.FLY, Probability.PROBABILITY_1);
			alert.sendAlert("O jogador " + player.getName() + " talvez esteja usando Fly");
		} else if (tick > 12 && tick <= 20) {
			alert.addAlert(player.getUniqueId(), Hack.FLY, Probability.PROBABILITY_2);
			alert.sendAlert("O jogador " + player.getName() + " est� provavelmente usando Fly");
		} else if (tick > 20) {
			alert.addAlert(player.getUniqueId(), Hack.FLY, Probability.PROBABILITY_3);
			alert.sendAlert("O jogador " + player.getName() + " est� definitivamente usando Fly");
		}
	}

}
