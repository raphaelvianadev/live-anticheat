package br.com.raphaelfury.anticheat.hack;

public enum Probability {
	
	PROBABILITY_1("talvez"), PROBABILITY_2("provavelmente"), PROBABILITY_3("definitivamente"); 
	
	private String probability;
	
	private Probability(String probability) {
		this.probability = probability;
	}
	
	public String getProbability() {
		return probability;
	}

}
