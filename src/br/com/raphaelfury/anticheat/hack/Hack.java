package br.com.raphaelfury.anticheat.hack;

public enum Hack {
	
	AUTO_SOUP("AutoSoup", "O jogador %player% est� utilizando AutoSoup [%level%]", 10),
	FLY("Fly", "O jogador %player% est� %probability% utilizando Fly", 10),
	FAST_BOW("Fly", "O jogador %player% est� %probability% utilizando Fly", 10);
	
	private String name, message;
	private Integer maxAlerts;
	
	private Hack(String name, String message, Integer maxAlerts) {
		this.name = name;
		this.message = message;
		this.maxAlerts = maxAlerts;
	}
	
	public String getName() {
		return name;
	}
	
	public String getMessage() {
		return message;
	}
	
	public Integer getMaxAlerts() {
		return maxAlerts;
	}

}
