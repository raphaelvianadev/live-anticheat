package br.com.raphaelfury.anticheat;

import org.bukkit.plugin.java.JavaPlugin;

import br.com.raphaelfury.anticheat.manager.Manager;

public class LiveAntiCheat extends JavaPlugin {
	
	protected static Manager manager;
	
	@Override
	public void onEnable() {
		manager = new Manager();
		
		System.out.println("Initialized.");
	}
	
	public static Manager getManager() {
		return manager;
	}

}
